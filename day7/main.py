import re
from typing import List, Tuple

REGEX_PATTERNS = {
    "change_dir": r"(\$\ cd\ )(.*)",
    "list_items": r"(\$\ ls)",
    "dir_type": r"(dir\ )(.*)",
    "file_type": r"([0-9]*)(\ )(.*)",
}

ROOT_DIR = "/"

PARENT_DIR = ".."

DIR_NODE_TYPE = "dir"
FILE_NODE_TYPE = "file"


class Node:
    node_type = DIR_NODE_TYPE
    node_size: int = 0
    children = []
    parent = None
    name: str

    def __init__(self, node_type: str, node_size: int, name: str, parent) -> None:
        self.node_type = node_type
        self.node_size = node_size
        self.name = name
        self.parent = parent
        self.children = []

    def add_child(self, child: "Node"):
        self.children.append(child)
        self.rec_update_size()

    def rec_update_size(self):
        self.node_size = 0
        for child in self.children:
            self.node_size += child.node_size
        if self.parent is not None:
            self.parent.rec_update_size()

    def set_parent(self, parent: "Node"):
        self.parent = parent

    def find_child_by_name(self, name: str):
        for child in self.children:
            if child.name == name:
                return child
        return None


def is_dir(row) -> Tuple[str, bool]:
    matches = re.match(REGEX_PATTERNS.get("dir_type"), row)
    if matches is not None:
        return matches.groups()[1], True
    return "", False


def is_file(row) -> Tuple[str, int, bool]:
    matches = re.match(REGEX_PATTERNS.get("file_type"), row)
    if matches is not None:
        return matches.groups()[2], int(matches.groups()[0]), True
    return "", None, False


def solve():
    with open("input.txt") as fp:
        ls_mode = False
        current_node = None
        root_node = None
        stack = []
        for row in fp:
            # Found `cd` command
            matches = re.match(REGEX_PATTERNS.get("change_dir"), row)
            if matches is not None:
                directory = matches.groups()[1]

                if directory == PARENT_DIR:
                    current_node = current_node.parent
                else:
                    # Root node
                    if directory == ROOT_DIR:
                        node = Node(DIR_NODE_TYPE, 0, ROOT_DIR, None)
                        root_node = node
                    else:
                        node = current_node.find_child_by_name(directory)
                    current_node = node

                ls_mode = False
                continue

            # Found `ls` command
            matches = re.match(REGEX_PATTERNS.get("list_items"), row)
            if matches is not None:
                ls_mode = True
                continue

            # LS mode
            if ls_mode:
                dir_name, dir_bool = is_dir(row)
                if dir_bool:
                    current_node.add_child(
                        Node(DIR_NODE_TYPE, 0, dir_name, current_node)
                    )

                file_name, file_size, file_bool = is_file(row)
                # print(file_size)
                if file_bool:
                    current_node.add_child(
                        Node(FILE_NODE_TYPE, file_size, file_name, current_node)
                    )
                    continue
        result = []
        # Part 1
        find_at_most_100_000(result, root_node)
        print(sum(result))

        # Part 2
        result = []
        find_smallest_dir_to_delete(result, root_node, root_node)
        print(min(result))


def find_at_most_100_000(result, root):
    if root.node_type == DIR_NODE_TYPE:
        if root.node_size <= 100000:
            result.append(root.node_size)
    for child in root.children:
        find_at_most_100_000(result, child)


def find_smallest_dir_to_delete(result, current, root):
    if current.node_type == DIR_NODE_TYPE:
        if 70000000 - root.node_size + current.node_size >= 30000000:
            result.append(current.node_size)
    for child in current.children:
        find_smallest_dir_to_delete(result, child, root)


solve()
