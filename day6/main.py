def find_starter_of_packet(inp, till_num):
    for i in range(len(inp) - till_num):
        freq = dict()
        freq[inp[i]] = freq[inp[i]] + 1 if inp[i] in freq else 1
        
        for j in range(1,till_num):
            freq[inp[i+j]] = freq[inp[i+j]] + 1 if inp[i+j] in freq else 1

        
        all_diff = True
        for f in freq.keys():
            if freq[f] != 1:
                all_diff = False
                break
        if all_diff == True:
            return True,i+till_num
    return False, -1

with open("input.txt", "r") as fp:
    for row in fp:
        # Part 1
        res, index = find_starter_of_packet(row, 4) 
        if res == True:
            print(index)

        # Part 2
        res, index = find_starter_of_packet(row, 14) 
        if res == True:
            print(index)
