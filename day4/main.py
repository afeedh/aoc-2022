def create_range(inp):
    input = inp.split("-")
    start = int(input[0])
    end = int(input[1])
    range = list()
    while start <= end:
        range.append(str(start))
        start = start + 1
    return range


def is_sub_list(l, s):
    sub_set = False
    if s == []:
        sub_set = True
    elif s == l:
        sub_set = True
    elif len(s) > len(l):
        sub_set = False

    else:
        for i in range(len(l)):
            if l[i] == s[0]:
                n = 1
                while (n < len(s) and (i + n) < len(l)) and (l[i + n] == s[n]):
                    n += 1

                if n == len(s):
                    sub_set = True

    return sub_set


def get_overlapping_pairs(arr1, arr2):
    if len(arr1) == 0 or len(arr2) == 0:
        return 0

    count = 0

    # Start from the minimum of both arrays
    if arr1[0] <= arr2[0]:
        start = arr1[0]
    else:
        start = arr2[0]

    if arr1[-1] >= arr2[-1]:
        end = arr1[-1]
    else:
        end = arr2[-1]

    for i in range(start, end + 1):
        if str(i) in arr1 and i in arr2:
            count = count + 1

    return count


with open("input.txt") as fp:
    inp = list()
    res = 0
    for row in fp:
        rr = row.split(",")
        first = rr[0]
        second = rr[1]
        first_range = create_range(first)
        second_range = create_range(second)

        if is_sub_list(first_range, second_range) or is_sub_list(
            second_range, first_range
        ):
            res = res + 1
    print(res)
