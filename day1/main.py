result = []

with open("./input.txt", "r") as fp:
    rows = fp.read().split("\n\n")
    for row in rows[:-1]:
        red = 0
        for x in row.split("\n"):
            red = red + int(x)
        result.append(red)

print(max(result))
s = 0
for x in sorted(result, reverse=True)[:3]:
    s += x
print(s)
