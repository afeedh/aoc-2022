def get_priority(char):
    if char.islower():
        val = ord(char) - ord("a") + 1
    else:
        val = ord(char) - ord("A") + 1 + ord("z") - ord("a") + 1
    return val


def solve_p2():
    res = 0
    sets = [[]]
    count = 0
    for row in fp:
        count = count + 1
        if count > 3:
            # Reset
            count = 1
            sets.append([])
        last = sets[-1]
        last.append(row[:-1])
        sets[-1] = last

    for seti in sets:
        for char in seti[0]:
            if char in seti[1] and char in seti[2]:
                res = res + get_priority(char)
                break
    print(res)


def solve_p1():
    res = 0
    for row in fp:
        first = row[: int(len(row) / 2)]
        second = row[int(len(row) / 2) :]
        for char in first:
            if char in second:
                val = get_priority(char)
                res = res + val
                break

    print(res)


with open("input.txt", "r") as fp:
    solve_p2()
