# Part 1
def solve_p1():
    res = 0
    out = []
    game = dict()
    game["A"] = "B"
    game["B"] = "C"
    game["C"] = "A"

    strategy = dict()
    strategy["X"] = "A"
    strategy["Y"] = "B"
    strategy["Z"] = "C"

    points = dict()
    points["A"] = 1
    points["B"] = 2
    points["C"] = 3
    for row in fp:
        out.append(row)
    for o in out[:-1]:
        they = o.split()[0]
        me = strategy[o.split()[1]]
        res = res + points[me]
        if me == they:
            res = res + 3
        elif game[they] == me:
            res = res + 6
        else:
            res = res + 0

    print(res)


# Part 2
def solve_p2():
    res = 0
    out = []
    game = dict()
    game["A"] = "B"
    game["B"] = "C"
    game["C"] = "A"

    lose = dict()
    lose["A"] = "C"
    lose["B"] = "A"
    lose["C"] = "B"
    strategy = dict()
    strategy["X"] = "A"
    strategy["Y"] = "B"
    strategy["Z"] = "C"

    points = dict()
    points["A"] = 1
    points["B"] = 2
    points["C"] = 3
    for row in fp:
        out.append(row)
    for o in out:
        they = o.split()[0]
        me = o.split()[1]

        if me == "X":
            # Lose
            res = res + points[lose[they]] + 0
        elif me == "Y":
            # Draw
            res = res + points[they] + 3
        else:
            # Win
            res = res + points[game[they]] + 6

    print(res)


with open("input.txt", "r") as fp:
    solve_p2()
