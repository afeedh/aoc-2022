import re

with open("input.txt", "r") as fp:
   
    # arr = [
    #         ["Z", "N"],
    #         ["M", "C", "D"],
    #         ["P"]
    #         ]

    arr = [ 
           ["R", "S" , "L", "F", "Q"],
           ["N", "Z", "Q", "G", "P", "T"],
           ["S", "M", "Q", "B"],
           ["T", "G", "Z", "J", "H", "C", "B", "Q"],
           ["P", "H", "M", "B", "N", "F", "S"],
           ["P", "C", "Q", "N", "S", "L", "V", "G"],
           ["W", "C", "F"],
           ["Q", "H", "G", "Z", "W", "V", "P", "M"],
           ["G", "Z", "D", "L", "C", "N", "R"]
            ]
   
    res = ""
    for row in fp:
        matches = re.search(r"move\ (\d*)\ from\ (\d*)\ to\ (\d*)", row)
        # if matches is None:
        #     continue
        matches = matches.groups()
        amnt = int(matches[0])
        from_arr = int(matches[1])
        to_arr = int(matches[2])

        i = 0 
        pop_items = []
        while i < amnt:
            i += 1
            # if len(arr[from_arr - 1]) == 0:
            #     continue
            el = arr[from_arr - 1].pop()
            pop_items.append(el)

        pop_items.reverse()
        
        for item in pop_items:
            arr[to_arr - 1].append(item)


    for ar in arr:
        if len(ar) == 0:
            res = res + " "
        else:
            res = res + ar[-1]
    print(res)

