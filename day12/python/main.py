START_TOKEN = 'S'
END_TOKEN = 'E'

def find_token_index(matrix, token):
        for row_i in range(len(matrix)):
        for col_i in range(len(matrix[row_i])):
            if matrix[row_i][col_i] == token:
                return row_i, col_i


def solve_p1(matrix, s_i, s_j, e_i, e_j, depth, visited):
    col_len = len(matrix[0])
    row_len = len(matrix)


    visited.append(str(s_i)+"_" + str(s_j))
    print(visited)

    # Base condition
    if depth >= 200:
        return
  

    # Recursively visit the possible moves
    # Left
    if s_j - 1 >= 0 and ( matrix[s_i][s_j - 1] == START_TOKEN or str(s_i) + "_" + str(s_j - 1) not in visited):
        ord_diff_left = abs(ord(matrix[s_i][s_j - 1]) - ord(matrix[s_i][s_j]))

        if matrix[s_i][s_j - 1] == END_TOKEN:
            print(depth)
            # print(visited)
            return

        if matrix[s_i][s_j] == START_TOKEN or ord_diff_left <= 1:
            solve_p1(matrix, s_i, s_j - 1, e_i, e_j, depth + 1, visited.copy())
    
    # Right
    if s_j + 1 < col_len and ( matrix[s_i][s_j + 1] == START_TOKEN or str(s_i) + "_" + str(s_j + 1) not in visited):
        ord_diff_right = abs(ord(matrix[s_i][s_j + 1]) - ord(matrix[s_i][s_j]))

        if matrix[s_i][s_j + 1] == END_TOKEN:
            print(depth)
            # print(visited)
            return
        if  matrix[s_i][s_j] == START_TOKEN or ord_diff_right <= 1:
            solve_p1(matrix, s_i, s_j + 1, e_i, e_j, depth + 1, visited.copy())
    
    # Up
    if s_i - 1 >= 0 and str(s_i - 1) + "_" + str(s_j) not in visited:
        ord_diff_up = abs(ord(matrix[s_i - 1][s_j]) - ord(matrix[s_i][s_j]))

        if matrix[s_i - 1][s_j] == END_TOKEN:
            print(depth)
            # print(visited)
            return
        if matrix[s_i][s_j] == START_TOKEN or ord_diff_up <= 1:
            solve_p1(matrix, s_i - 1, s_j, e_i, e_j, depth + 1, visited.copy())

    # Down
    if s_i + 1 < row_len and (matrix[s_i - 1][s_j] == START_TOKEN or str(s_i + 1) + "_" + str(s_j) not in visited):
        ord_diff_down = abs(ord(matrix[s_i + 1][s_j]) - ord(matrix[s_i][s_j]))

        if matrix[s_i + 1][s_j] == END_TOKEN:
            print(depth)
            # print(visited)
            return
        if matrix[s_i][s_j] == START_TOKEN or ord_diff_down <= 1:
            solve_p1(matrix, s_i + 1, s_j, e_i, e_j, depth + 1, visited.copy())
    

with open("input.txt") as fp:
    matrix = []
    for row in fp:
        matrix.append([x for x in row[:-1]])
    

    s_i, s_j = find_token_index(matrix, START_TOKEN)
    e_i, e_j = find_token_index(matrix, END_TOKEN)


    solve_p1(matrix, s_i, s_j, e_i, e_j, 0, [])
    
