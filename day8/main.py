from typing import List


class Coord:
    def __init__(self, x: int, y: int) -> None:
        self.x = x
        self.y = y


def count_edges(n_rows: int, n_cols: int):
    return (n_cols * 2) + (n_rows - 2) * 2


def _is_visible_from_left(matrix: List[List], coord: Coord):
    found = False
    for i in range(0, coord.y):
        if matrix[coord.x][i] >= matrix[coord.x][coord.y]:
            found = True
            break

    if found:
        return False
    return True


def _is_visible_from_right(matrix: List[List], coord: Coord):
    found = False
    for i in range(coord.y + 1, len(matrix[0])):
        if matrix[coord.x][i] >= matrix[coord.x][coord.y]:
            found = True
            break

    if found:
        return False

    return True


def _is_visible_from_top(matrix: List[List], coord: Coord):
    found = False
    for i in range(0, coord.x):
        if matrix[i][coord.y] >= matrix[coord.x][coord.y]:
            found = True
            break

    if found:
        return False

    return True


def _is_visible_from_bottom(matrix: List[List], coord: Coord):
    found = False
    for i in range(coord.x + 1, len(matrix)):
        if matrix[i][coord.y] >= matrix[coord.x][coord.y]:
            found = True
            break

    if found:
        return False

    return True


def is_visible(matrix: List[List], coord: Coord):
    return (
        _is_visible_from_left(matrix, coord)
        or _is_visible_from_right(matrix, coord)
        or _is_visible_from_top(matrix, coord)
        or _is_visible_from_bottom(matrix, coord)
    )


def _sc_from_left(matrix: List[List], coord: Coord):
    count = 0
    for i in range(coord.y - 1, -1, -1):
        count += 1
        if matrix[coord.x][i] >= matrix[coord.x][coord.y]:
            break
    return count


def _sc_from_right(matrix: List[List], coord: Coord):
    count = 0
    for i in range(coord.y + 1, len(matrix[0])):
        count += 1
        if matrix[coord.x][i] >= matrix[coord.x][coord.y]:
            break
    return count


def _sc_from_top(matrix: List[List], coord: Coord):
    count = 0
    for i in range(coord.x - 1, -1, -1):
        count += 1
        if matrix[i][coord.y] >= matrix[coord.x][coord.y]:
            break
    return count


def _sc_from_bottom(matrix: List[List], coord: Coord):
    count = 0
    for i in range(coord.x + 1, len(matrix)):
        count += 1
        if matrix[i][coord.y] >= matrix[coord.x][coord.y]:
            break
    return count


def scenic_score(matrix: List[List], coord: Coord):
    return (
        _sc_from_left(matrix, coord)
        * _sc_from_bottom(matrix, coord)
        * _sc_from_right(matrix, coord)
        * _sc_from_top(matrix, coord)
    )


def solve():
    with open("input.txt") as fp:
        matrix: List[List] = []
        for row in fp:
            m_row = []
            for el in row[:-1]:
                m_row.append(int(el))
            matrix.append(m_row)
    # print(is_visible(matrix, Coord(1, 3)))
    # Part 1
    vis_count = 0
    for i in range(1, len(matrix) - 1):
        for j in range(1, len(matrix[i]) - 1):
            if is_visible(matrix, Coord(i, j)):
                vis_count += 1
    print(count_edges(len(matrix), len(matrix[0])) + vis_count)

    # Part 2
    scores = []
    for i in range(1, len(matrix) - 1):
        for j in range(1, len(matrix[i]) - 1):
            scores.append(scenic_score(matrix, Coord(i, j)))
    print(max(scores))


solve()
